//converts Prices of products from GBP to US Dollars
function GBtoDollar() {
    let p1 = document.getElementById("product1");
    let str = "$110";
    p1.innerHTML = str.bold();
    let p2 = document.getElementById("product2");
    str = "$95";
    p2.innerHTML = str.bold();
    let p3 = document.getElementById("product3");
    str = "$120";
    p3.innerHTML = str.bold();
    let p4 = document.getElementById("product4");
    str = "$80";
    p4.innerHTML = str.bold();
    let p5 = document.getElementById("product5");
    str = "$70";
    p5.innerHTML = str.bold();
    let p6 = document.getElementById("product6");
    str = "$120";
    p6.innerHTML = str.bold();
}

//converts Prices of products from US dollars to GBP
function dollarToGB() {
    let p1 = document.getElementById("product1");
    let str = "£80";
    p1.innerHTML = str.bold();
    let p2 = document.getElementById("product2");
    str = "£70";
    p2.innerHTML = str.bold();
    let p3 = document.getElementById("product3");
    str = "£90";
    p3.innerHTML = str.bold();
    let p4 = document.getElementById("product4");
    str = "£60";
    p4.innerHTML = str.bold();
    let p5 = document.getElementById("product5");
    str = "£50";
    p5.innerHTML = str.bold();
    let p6 = document.getElementById("product6");
    str = "£90";
    p6.innerHTML = str.bold();
}

//opens search window in place of current tab
function search(){
    window.open("search.html","_self")
}